# ATEX
* This is a replication package for "Security Wisdom of the Crowd: Automated Attack Tree Synthesis from Online Discussions". 
* Some code and data are partially open sourced due to the confidentiality, and will be continuously updated.
* Our project is public at:
[https://gitlab.com/atex2/atex](https://gitlab.com/atex2/atex).

## Content
[1 Get Started](#1-get-started)<br>
&ensp;&ensp;[1.1 Requirements](#11-requirements)<br>
&ensp;&ensp;[1.2 Dataset](#12-dataset)<br>
&ensp;&ensp;[1.3 Train and Test](#13-train-and-test)<br>
[2 Project Summary](#2-project-summary)<br>
[3 Approach](#3-approach)<br>
[4 Experiments](#4-experiments)<br>
[5 Experimental Results](#5-experimental-results)<br>
&ensp;&ensp;[5.1 RQ1: Performance on Attack Tree Synthesizing](#51-rq1-performance-on-attack-tree-synthesizing)<br>
&ensp;&ensp;[5.2 RQ2: Performance on Event Extraction](#52-rq2-performance-on-event-extraction)<br>
&ensp;&ensp;[5.3 RQ3: Performance on Relation Extraction](#53-rq3-performance-on-relation-extraction)<br>
[6 Human Evaluation and Practical Application](#6-human-evaluation-and-practical-application)<br>
&ensp;&ensp;[6.1 Human Evaluation](#61-human-evaluation)<br>
&ensp;&ensp;[6.2 Practical Application](#62-practical-application)<br>

## 1 Get Started
### 1.1 Requirements
* Hardwares: NVIDIA GeForce RTX 2060 GPU, intel core i5 CPU
* OS: Ubuntu 20.04, Windows 7, 8, 10, 11
* Packages: 
  * python 3.6
  * pytorch, pytorch-transformers 1.9.0
  * cuda 11.1
  * python 2.7 (for evaluation)

### 1.2 Dataset
ATEX is evaluated on Stack Overflow [data](./data). 
The structures of security posts are as follows:
* train/valid/test
  * Trigger: Event trigger
  * Instrument: Argument-instrument
  * Target: Argument-target


### 1.3 Train and Test
1. Go the [src](./src) directory, process the dataset:
```
python data.py
```
2. Train ATEX model:
```
python train.py
```


## 2 Project Summary
Security has become a key focus in software development and maintenance. When having security concerns, developers tend to discuss with the crowd about possible attack events, including attack targets and how attacks might succeed, via knowledge-sharing platforms, e.g., Stack Overflow. Such online discussions usually hold rich information for synthesizing attack trees, i.e., attack goals and possible attack methods, which are invaluable for other developers to analyze possible attacks regarding similar assets. In this paper, we propose ATEX, an automated approach to synthesizing attack trees from security posts of Stack Overflow based on a transition-based event and relation extraction. Specifically, ATEX extracts the attack events and their relations simultaneously from the sentences within a limited scope based on a transition-based joint event and relation extraction model, and utilizes a set of heuristic rules to automatically assemble the attack events and relations into attack trees. To evaluate the performance of ATEX, we conduct experiments on 5,070 security posts collected from Stack Overflow. The experimental results show that ATEX achieves the highest tree similarity in attack tree synthesizing, with 15.03% AHD and 13.24% TEDS. ATEX also outperforms all baselines in both event and relation extraction, with 76.41% and 84.34% F1 scores, respectively. Moreover, ATEX has been successfully adopted by Company Z, a Fortune Global 500 Company, to enrich their attack database, which enlarges the volume of their attack tree database by 42.32%. These results demonstrate that ATEX can utilize crowd security knowledge to facilitate the development of secure software.

The major contributions of this paper are summarized as follows:
1) Technique: ATEX, an automated approach to synthesize
attack trees based on joint event and relation extraction.
To the best of our knowledge, this is the first work on
automatically synthesizing attack trees from security posts
of a knowledge-sharing platform.
2) Evaluation: An experimental evaluation of the performance of ATEX against state-of-the-art baselines, which
shows that ATEX outperforms all baselines.
3) Application: A successful application of ATEX on enriching a Fortune Global 500 company’s security knowledge
base, which further demonstrates ATEX’s practicality.
4) Data: A public release of the dataset with 1,354 attack trees,
and source code to facilitate the replication of our study
and its application in extensive contexts.


## 3 Approach
1. The model of diagram is available at [diagram](./diagram). The structure is shown as follows:
![Image text](diagram/model.png)
2. The definitions of transition-based event and relation extraction model are shown as follows.
* The definition of 10 states:
![Image text](diagram/state_define.png)
* The definition of 12 transition actions:
![Image text](diagram/action_define.png)
* To illustrate the states and actions more Intuitively, we illustrate the ground-truth transition example:
![Image text](diagram/transition_example.png)
3. Labeling process: 
* Label the BIO (Begin+Inside+Other) tags, event and relation tags
  * First, label the trigger and argument with BIO tags, e.g., the "copy" in the prvious example is labeled as "B-TRG"; the "cookie" is labeled as "B-INS"; the "information" is labeld as "B-TAR".
  * Second, label the dependency with position pairs, e.g., in the previous example, (2,3) and (2,8) are dependencies for attack event "copy cookie to attack information".
  * Third, label the relation with position pairs between event triggers based on triplet, e.g., in the previous example, (2,5,And), (9,5,Parent) and (9,2,Parent) are relations.
* Label the ground-truth actions with the rags, e.g., in the previous example, the labeld action sequence is [O-DELETE, GEN-SHIFT, GEN-TRG,...,NO-SHIFT].

## 4 Experiments
3 RQs are proposed by our paper, which is related to our experiment:

- RQ1: What is the performance of ATEX in extracting
events from security posts?
- RQ2: What is the performance of ATEX in extracting
relations between attack events?
- RQ3: What effect the attack sentence classifier might
have on the performance of event and relation extraction?

For RQ1 and RQ2, we present the source paper of common baseline.
- Structured-Joint: [https://doi.org/10.18653/v1/D19-1041](https://doi.org/10.18653/v1/D19-1041)



For RQ1, we present the source paper of different baselines.
- DMCNN: [https://aclanthology.org/P15-1017](https://aclanthology.org/P15-1017)
- JMEE: [https://doi.org/10.18653/v1/d18-1156](https://doi.org/10.18653/v1/d18-1156)
- DEEB-RNN: [https://aclanthology.org/P18-2066/](https://aclanthology.org/P18-2066/)
- ED3C: [https://doi.org/10.18653/v1/2021.emnlp-main.439](https://doi.org/10.18653/v1/2021.emnlp-main.439)
- MLBiNet: [https://doi.org/10.18653/v1/2021.acl-long.373](https://doi.org/10.18653/v1/2021.acl-long.373)

For RQ2, we present the source paper of different baselines:
- CogCompTime: [https://aclanthology.org/D18-2013]( https://aclanthology.org/D18-2013)
- Perceptron: [https://aclanthology.org/P18-1122](https://aclanthology.org/P18-1122)
- KnowledgeILP: [https://doi.org/10.18653/v1/D19-1642](https://doi.org/10.18653/v1/D19-1642)
- JC Learning: [https://doi.org/10.18653/v1/2020.emnlp-main.51](https://doi.org/10.18653/v1/2020.emnlp-main.51)

For RQ3,  we analyze the contribution of sentence classifier with the following subtasks: 
1. Compare ATEX with "w/o _**SEL**_", which removes the attack sentence classifier features from ATEX;
2. Analyze the _K_'s contribution to the event and relation extraction.

## 5 Experimental Results
### 5.1 RQ1: Performance on Attack Tree Synthesizing
![Image text](diagram/rq1.png)


**Answering RQ1**: ATEX outperforms the SOTA baseline in synthesizing the attack trees. It reaches the lowest AHD and TEDS at 15.03% and 13.24%, indicating that the attack trees generated by ATEX are much more similar to the ground truth. The qualitative analysis further illustrates the effectiveness of ATEX on the attack tree synthesizing.

### 5.2 RQ2: Performance on Event Extraction
![Image text](diagram/rq2.png)


**Answering RQ2**: ATEX outperforms the five baselines in extracting
attack events across the trigger and argument detection, and the
average Precision, Recall, and F1 are 75.22%, 77.98%, 76.41%, outperforming the best baseline by 5.52% (Pre.), 4.20% (Rec.), 4.85% (F1).

_Moreover, we analyze the performance of action prediction. This is an additional analysis. Since action prediction corresponds to the results of event relation extraction, this result is equivalent to RQ1-2._
![Image text](diagram/action.png)

### 5.3 RQ3: Performance on Relation Extraction
![Image text](diagram/rq3.png)


**Answering RQ3**: ATEX outperforms the four baselines in extracting
And, Or and AchievedBy relations, and the average Precision,
Recall, F1 are 81.73%, 87.90%, 84.34%, outperforming the best baseline
by 6.46% (Pre.), 17.04% (Rec.), 11.90% (F1).


## 6 Human Evaluation and Practical Application
To demonstrate the practicality of ATEX, we have successfully applied ATEX in enhancing the attack tree database of
Company Z, which is a Fortune Global 500 company.
We present the result of human evaluation and practical application.


### 6.1 Human Evaluation
We invite three security practitioners in Company Z with more than 10-years of security-related work experience,
and they care about the attacks in two domains (i.e., Web Application and Database). Then, we collect 192 security posts, dated from
Oct. 1st, 2022 to Jan. 1st, 2023 (Note that these are never used in the
model training or testing) . We ask the practitioners to manually inspect and rate
these attack trees with the following criteria:

1. Accurateness:
Whether the attack tree is accurate to describe a real attack in
software security.

2. Adequateness: Whether the attack tree is
adequate enough to describe the details of attacks.

3. Usefulness:
Whether the attack tree is useful for enhancing secure software
development.

The result is shown as follows:

![Image text](diagram/human.png)

We can see that, the maximum distribution score of ATEX is all over 3 on the three criteria,
which is higher than the Structured-Joint+ (the maximum distribution score of Structured-Joint+ is only 2). The average scores of
ATEX are 3.7, which is also higher than the Structured-Joint+ (2.0).
These results indicate that practitioners are generally satisfied with
the attack trees extracted by ATEX.

Furthermore, we interview the practitioners regarding the perceived usefulness of ATEX, and they indicate that ATEX is quite
helpful. Here are two representative comments:

>_One of the attack methods from the Format-String attack tree provided by ATEX is ‘execute harmful code to attack buffer string in
printf(buffer)’. When reviewing this, I just realized that my previous
code are also vulnerable due to unsafely using the ‘printf’ function.
To make our colleges be more cautious, I added this information
into our security knowledge base._

>_Spectre attacks can be exploited to critical vulnerabilities by stealing
data in the processors we are using now. I did not notice this until
learning the attack trees recommended by ATEX._

### 6.2 Practical Application

we asked the security practitioners
to manually inspect the synthesized 121 attack trees, and finally
select trees to replenish their own attack knowledge database. The results are shown as follows:

![Image text](diagram/application2.png)

We leverage the security knowledge from
ATEX to enhance their database. Given the 121 attack trees,
the security experts manually inspected and finally selected
102 attack trees to replenish their own attack knowledge base.
In total, 102 out of 121 synthesized attack trees are accepted
with an acceptance ratio of 84.30%, as shown in Table V, and
the overall increment ratio for the entire database is 42.32%.


